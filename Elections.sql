DROP TABLE IF EXISTS Election;

CREATE TABLE Election(
	Electionid INT PRIMARY KEY
	,MunicipaliteId INT REFERENCES Municipalite(MunicipaliteId)
	,DateElection DATE NOT NULL
	,Actif BIT DEFAULT 1
);

INSERT INTO Election VALUES(1, 1023, '2026-05-01', 1);
INSERT INTO Election VALUES(2, 1023, '2030-05-01', 1);
INSERT INTO Election VALUES(3, 23027, '2026-05-01', 1);

SELECT * FROM Election;