DROP TABLE IF EXISTS dbo.Municipalite;

CREATE TABLE Municipalite (
		MunicipaliteId INT PRIMARY KEY
		,Nom VARCHAR(100) NOT NULL
		,Courriel VARCHAR(250) NOT NULL
		,SiteWeb VARCHAR(250)
		,DateProchaineElection DATE
		,Actif BIT NOT NULL DEFAULT 1
);

SELECT * FROM Municipalite;