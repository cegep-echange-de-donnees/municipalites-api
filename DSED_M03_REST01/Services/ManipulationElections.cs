﻿namespace DSED_M03_REST01.Services
{
    public class ManipulationElections
    {
        private IDepotElections m_depotElections;
        private IDepotMunicipalites m_depotMunicipalites;
        public ManipulationElections(IDepotElections p_depotElections, IDepotMunicipalites p_depotMunicipalites)
        {
            if(p_depotElections == null)
            {
                throw new ArgumentNullException(nameof(p_depotElections));
            }
            if (p_depotMunicipalites == null)
            {
                throw new ArgumentNullException(nameof(p_depotMunicipalites));
            }

            this.m_depotElections = p_depotElections;
            this.m_depotMunicipalites = p_depotMunicipalites;
        }

        public Election RechercherElectionParIdentifiant(int p_municipaliteId, int p_electionId)
        {
            if(p_electionId < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(p_electionId));
            }
            if(p_municipaliteId < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(p_municipaliteId));
            }
            if(this.m_depotMunicipalites.ChercherMunicipaliteParCodeGeographique(p_municipaliteId) == null)
            {
                throw new ArgumentNullException(nameof(p_municipaliteId));
            }

            return this.m_depotElections.RechercherElectionParIdentifiant(p_electionId);
        }

        public IEnumerable<Election> RechercherElectionsPourMunicipalite(int p_municipaliteId)
        {
            if(p_municipaliteId < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(p_municipaliteId));
            }
            if(this.m_depotMunicipalites.ChercherMunicipaliteParCodeGeographique(p_municipaliteId) == null)
            {
                throw new ArgumentNullException(nameof(p_municipaliteId));
            }

            return this.m_depotElections.RechercherElectionsPourMunicipalite(p_municipaliteId);
        }

        public void DesactiverElection(Election p_election)
        {
            if(p_election == null)
            {
                throw new ArgumentNullException(nameof(p_election));
            }

            this.m_depotElections.DesactiverElection(p_election);
        }

        public void AjouterElection(Election p_election)
        {
            if (p_election == null)
            {
                throw new ArgumentNullException(nameof(p_election));
            }

            DateTime date = DateTime.Now;

            if(p_election.DateElection < date)
            {
                throw new ArgumentOutOfRangeException(nameof(p_election.DateElection));
            }
            if(this.m_depotMunicipalites.ChercherMunicipaliteParCodeGeographique(p_election.MunicipaliteId) == null)
            {
                throw new ArgumentNullException(nameof(p_election.MunicipaliteId));
            }

            this.m_depotElections.AjouterElection(p_election);
        }

        public void MAJElection(Election p_election)
        {
            if (p_election == null)
            {
                throw new ArgumentNullException(nameof(p_election));
            }

            DateTime date = DateTime.Now;

            if (p_election.DateElection < date)
            {
                throw new ArgumentOutOfRangeException(nameof(p_election.DateElection));
            }
            if(this.m_depotMunicipalites.ChercherMunicipaliteParCodeGeographique(p_election.MunicipaliteId) == null)
            {
                throw new ArgumentNullException(nameof(p_election.MunicipaliteId));
            }

            this.m_depotElections.MAJElection(p_election);
        }
        public IEnumerable<Services.Election> ListerElections()
        {
            return this.m_depotElections.ListerElections();
        }

    }
}
