﻿namespace DSED_M03_REST01.Services
{
    public class Municipalite
    {
        public int MunicipaliteId { get; private set; }
        public string Nom { get; private set; }
        public string Courriel { get; private set; }
        public string? SiteWeb { get; private set; }
        public DateTime? DateProchaineElection { get; private set; }
        public bool Actif { get; set; }
        private List<Election>? m_Elections;
        public List<Election>? Elections
        {
            get
            {
                return m_Elections;
            }
            set
            {
                m_Elections = value;
            }
        }
        public Municipalite(int p_municipaliteId, string p_nom, string p_courriel, string? p_siteWeb, DateTime? p_dateProchaineElection, bool p_actif, List<Election>? p_elections)
        {
            if(p_municipaliteId < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(p_municipaliteId));
            }
            if(p_nom == null)
            {
                throw new ArgumentNullException(nameof(p_nom));
            }
            if(p_courriel == null)
            {
                throw new ArgumentNullException(nameof(p_courriel));
            }
            this.MunicipaliteId = p_municipaliteId;
            this.Nom = p_nom;
            this.Courriel = p_courriel;
            this.SiteWeb = p_siteWeb;
            this.DateProchaineElection = p_dateProchaineElection;
            this.Actif = p_actif;
            this.Elections = p_elections;
        }
        public override string ToString()
        {
            return $"Code Geographique: {this.MunicipaliteId}\r\nNom: {this.Nom}\r\nCourriel: {this.Courriel}\r\n" +
                $"SiteWeb: {(this.SiteWeb == null ? "Aucun site web référencé" : this.SiteWeb)}\r\n" +
                $"Date prochaine election: {(this.DateProchaineElection == null ? "Aucune date d'élection connue" : this.DateProchaineElection.Value.Day + "/" + this.DateProchaineElection.Value.Month + "/" + this.DateProchaineElection.Value.Year)}\r\n";
        }
        public override bool Equals(object? obj)
        {
            if(obj is not Municipalite)
            {
                return false;
            }

            Municipalite autre = obj as Municipalite;
            return this.MunicipaliteId == autre.MunicipaliteId
                && this.Nom == autre.Nom
                && this.Courriel == autre.Courriel
                && this.SiteWeb == autre.SiteWeb
                && this.DateProchaineElection == autre.DateProchaineElection
                && this.Actif == autre.Actif
                && this.Elections == autre.Elections;
        }
        public override int GetHashCode()
        {
            return this.GetHashCode();
        }
    }
}