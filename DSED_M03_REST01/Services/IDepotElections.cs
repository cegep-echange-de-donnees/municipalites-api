﻿namespace DSED_M03_REST01.Services
{
    public interface IDepotElections
    {
        public Election RechercherElectionParIdentifiant(int p_electionId);
        public IEnumerable<Election> RechercherElectionsPourMunicipalite(int p_municipaliteId);
        public void DesactiverElection(Election p_election);
        public void AjouterElection(Election p_election);
        public void MAJElection(Election p_election);
        public IEnumerable<Election> ListerElections();
    }
}
