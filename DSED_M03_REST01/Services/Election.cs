﻿namespace DSED_M03_REST01.Services
{
    public class Election
    {
        public int ElectionId { get; private set; }
        public int MunicipaliteId { get; private set; }
        public DateTime? DateElection { get; private set; }
        public bool Actif { get; set; }
        public Election(int p_electionId, int p_municipaliteId, DateTime? p_dateElection, bool p_actif)
        {
            if(p_electionId < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(p_electionId));
            }
            if(p_municipaliteId < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(p_municipaliteId));
            }
            this.ElectionId = p_electionId;
            this.MunicipaliteId = p_municipaliteId;
            this.DateElection = p_dateElection;
            this.Actif = p_actif;
        }
        public override string ToString()
        {
            return $"Code Geographique: {this.MunicipaliteId}\r\nDate de l'élection: {this.DateElection}";
        }
        public override bool Equals(object? obj)
        {
            if(obj is not Election)
            {
                return false;
            }

            Election autre = obj as Election;

            return autre.ElectionId == this.ElectionId &&
                   autre.MunicipaliteId == this.MunicipaliteId &&
                   autre.DateElection == this.DateElection &&
                   autre.Actif == this.Actif;
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
