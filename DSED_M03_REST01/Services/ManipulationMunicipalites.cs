﻿namespace DSED_M03_REST01.Services
{
    public class ManipulationMunicipalites
    {
        private IDepotMunicipalites m_depotMunicipalites;
        private IDepotElections m_depotElections;
        public ManipulationMunicipalites(IDepotMunicipalites p_depotMunicipalites, IDepotElections p_depotElections)
        {
            if(p_depotMunicipalites == null)
            {
                throw new ArgumentNullException(nameof(p_depotMunicipalites));
            }
            if (p_depotElections == null)
            {
                throw new ArgumentNullException(nameof(p_depotElections));
            }

            m_depotMunicipalites = p_depotMunicipalites;
            m_depotElections = p_depotElections;
        }
        public Municipalite ChercherMunicipaliteParCodeGeographique(int p_municipaliteId)
        {
            if(p_municipaliteId < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(p_municipaliteId));
            }

            Municipalite municipalite = this.m_depotMunicipalites.ChercherMunicipaliteParCodeGeographique(p_municipaliteId);

            if(municipalite != null)
            {
                this.RechercherElectionsPourMunicipalite(municipalite);
            }

            return municipalite;
        }

        public IEnumerable<Municipalite> ListerMunicipalitesActives()
        {
            List<Municipalite> municipalites = this.m_depotMunicipalites.ListerMunicipalitesActives().ToList();
            municipalites?.ForEach(municipalite => this.RechercherElectionsPourMunicipalite(municipalite));
            return municipalites;
        }

        public void DesactiverMunicipalite(Municipalite p_municipalite)
        {
            if(p_municipalite == null)
            {
                throw new ArgumentNullException(nameof(p_municipalite));
            }

            this.DesactiverElections(p_municipalite.Elections);
            this.m_depotMunicipalites.DesactiverMunicipalite(p_municipalite);
        }

        public void AjouterMunicipalite(Municipalite p_municipalite)
        {
            if (p_municipalite == null)
            {
                throw new ArgumentNullException(nameof(p_municipalite));
            }

            List<Election>? elections = p_municipalite?.Elections;
            p_municipalite.Elections = null;

            this.m_depotMunicipalites.AjouterMunicipalite(p_municipalite);

            if(elections != null)
            {
                this.AjouterElections(elections);
            }
        }

        public void MAJMunicipalite(Municipalite p_municipalite)
        {
            if (p_municipalite == null)
            {
                throw new ArgumentNullException(nameof(p_municipalite));
            }

            this.VerifierElections(p_municipalite);
            this.m_depotMunicipalites.MAJMunicipalite(p_municipalite);
        }
        private void RechercherElectionsPourMunicipalite(Municipalite p_municipalite)
        {
            p_municipalite.Elections = this.m_depotElections.RechercherElectionsPourMunicipalite(p_municipalite.MunicipaliteId).ToList();
        }
        private void AjouterElections(List<Election> p_elections)
        {
            p_elections?.ForEach(election => this.m_depotElections.AjouterElection(election));
        }
        public int ObtenirElectionIdMax()
        {
            int electionIdMax = this.m_depotElections.ListerElections()
                                                     .OrderByDescending(election => election.ElectionId)
                                                     .FirstOrDefault()?.ElectionId ?? 0;

            return electionIdMax;
        }
        private void VerifierElections(Municipalite p_municipalite)
        {
            if (p_municipalite.Elections != null)
            {
                Dictionary<int, Services.Election> electionsAnciennes = this.m_depotElections.RechercherElectionsPourMunicipalite(p_municipalite.MunicipaliteId)
                                                                                                    .ToDictionary(election => election.ElectionId);

                foreach (Services.Election electionAJour in p_municipalite.Elections)
                {
                    if ((electionsAnciennes != null && !(electionsAnciennes.ContainsKey(electionAJour.ElectionId))) || electionsAnciennes == null)
                    {
                        if(this.m_depotElections.RechercherElectionParIdentifiant(electionAJour.ElectionId) != null)
                        {
                            this.m_depotElections.MAJElection(electionAJour);
                        }
                        else
                        {
                            this.m_depotElections.AjouterElection(electionAJour);
                        }
                    }
                    else if (electionsAnciennes != null && electionsAnciennes.ContainsKey(electionAJour.ElectionId))
                    {
                        if (!(electionAJour.Equals(electionsAnciennes[electionAJour.ElectionId])))
                        {
                            this.m_depotElections.MAJElection(electionAJour);
                            electionsAnciennes.Remove(electionAJour.ElectionId);
                        }
                    }
                }

                p_municipalite.Elections = electionsAnciennes.Values.ToList();

                if(p_municipalite.Elections != null && p_municipalite.Elections.Count > 0)
                {
                    this.DesactiverElections(p_municipalite.Elections);
                }

            }
        }
        private void DesactiverElections(List<Election>? p_elections)
        {
            p_elections?.ForEach(election => this.m_depotElections.DesactiverElection(election));
            p_elections = null;

        }
    }
}
