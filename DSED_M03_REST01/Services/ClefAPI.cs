﻿namespace DSED_M03_REST01.Services
{
    public class ClefAPI
    {
        public Guid ClefAPIId { get; set; }
        public ClefAPI()
        {
            this.ClefAPIId = Guid.NewGuid();
        }
        public ClefAPI(Guid p_clefAPIId)
        {
            if(p_clefAPIId == Guid.Empty)
            {
                throw new ArgumentNullException(nameof(p_clefAPIId));
            }
            this.ClefAPIId = p_clefAPIId;
        }
        public override bool Equals(object? obj)
        {
            if(obj is not ClefAPI)
            {
                return false;
            }

            ClefAPI autre = obj as ClefAPI;
            return this.ClefAPIId == autre.ClefAPIId;
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
