﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSED_M03_REST01.Services
{
    public interface IDepotMunicipalites
    {
        public Municipalite ChercherMunicipaliteParCodeGeographique(int p_municipaliteId);
        public IEnumerable<Municipalite> ListerMunicipalitesActives();
        public void DesactiverMunicipalite(Municipalite p_municipalite);
        public void AjouterMunicipalite(Municipalite p_municipalite);
        public void MAJMunicipalite(Municipalite p_municipalite);
    }
}
