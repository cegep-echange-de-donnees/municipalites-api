﻿using DSED_M03_REST01.Filters;
using DSED_M03_REST01.Models;
using DSED_M03_REST01.Services;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DSED_M03_REST01.Controllers
{
    [ApiKey()]
    [Route("api/municipalites")]
    [ApiController]
    public class MunicipalitesController : ControllerBase
    {
        private ManipulationMunicipalites m_manipulationMunicipalites;
        public MunicipalitesController(ManipulationMunicipalites p_manipulationMunicipalites)
        {
            if(p_manipulationMunicipalites == null)
            {
                throw new ArgumentNullException(nameof(p_manipulationMunicipalites));
            }

            this.m_manipulationMunicipalites = p_manipulationMunicipalites;
        }
        // GET: api/<MunicipalitesController>
        [HttpGet]
        [ProducesResponseType(200)]
        public ActionResult<IEnumerable<Models.Municipalite>> Get()
        {
            List<Models.Municipalite> listeMunicipalites = this.m_manipulationMunicipalites.ListerMunicipalitesActives()
                                                                                           .Select(municipalite => new Models.Municipalite(municipalite))
                                                                                           .ToList();

            return Ok(listeMunicipalites);
        }

        // GET api/<MunicipalitesController>/5
        [HttpGet("{municipaliteId}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public ActionResult<Models.Municipalite> Get(int municipaliteId)
        {
            Services.Municipalite municipaliteSRV = this.m_manipulationMunicipalites.ChercherMunicipaliteParCodeGeographique(municipaliteId);

            if(municipaliteSRV != null)
            {
                Models.Municipalite municipaliteMODEL = new Models.Municipalite(municipaliteSRV);

                return Ok(municipaliteMODEL);
            }

            return NotFound();
        }

        // POST api/<MunicipalitesController>
        [HttpPost]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        public ActionResult Post([FromBody] Models.Municipalite municipalite)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest();
            }

            int municipaliteIdMax = this.m_manipulationMunicipalites.ListerMunicipalitesActives()
                                                                    .OrderByDescending(municipalite => municipalite.MunicipaliteId)
                                                                    .FirstOrDefault()?.MunicipaliteId ?? 0;

            municipalite.MunicipaliteId = municipaliteIdMax + 1;

            if(municipalite.Elections != null)
            {
                int electionIdMax = this.m_manipulationMunicipalites.ObtenirElectionIdMax();

                foreach(Models.Election election in municipalite.Elections)
                {
                    electionIdMax += 1;
                    election.ElectionId = electionIdMax;
                    election.MunicipaliteId = municipalite.MunicipaliteId;
                }
            }

            Services.Municipalite municipaliteSRV = municipalite.VersEntite();

            this.m_manipulationMunicipalites.AjouterMunicipalite(municipaliteSRV);

            return CreatedAtAction(nameof(Get), new { id = municipalite.MunicipaliteId }, municipalite);
        }

        // PUT api/<MunicipalitesController>/5
        [HttpPut("{municipaliteId}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public ActionResult Put(int municipaliteId, [FromBody] Models.Municipalite municipalite)
        {
            if (!ModelState.IsValid || municipalite.MunicipaliteId != municipaliteId)
            {
                return BadRequest();
            }

            int index = this.m_manipulationMunicipalites.ChercherMunicipaliteParCodeGeographique(municipaliteId)?.MunicipaliteId ?? -1;

            if (index < 0)
            {
                return NotFound();
            }

            Services.Municipalite municipaliteSRV = municipalite.VersEntite();
            
            this.m_manipulationMunicipalites.MAJMunicipalite(municipaliteSRV);

            return NoContent();
        }

        // DELETE api/<MunicipalitesController>/5
        [HttpDelete("{municipaliteId}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        public ActionResult Delete(int municipaliteId)
        {
            Services.Municipalite? municipalite = this.m_manipulationMunicipalites.ListerMunicipalitesActives()
                                                                                 .Where(municipalite => municipalite.MunicipaliteId == municipaliteId)
                                                                                 .SingleOrDefault();

            if(municipalite == null)
            {
                return NotFound();
            }

            this.m_manipulationMunicipalites.DesactiverMunicipalite(municipalite);

            return NoContent();
        }
    }
}
