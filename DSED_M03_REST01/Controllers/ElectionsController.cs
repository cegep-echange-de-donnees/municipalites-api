﻿using DSED_M03_REST01.Filters;
using DSED_M03_REST01.Services;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DSED_M03_REST01.Controllers
{
    [ApiKey()]
    [Route("api/municipalites/{municipaliteId}/Elections")]
    [ApiController]
    public class ElectionsController : ControllerBase
    {
        private ManipulationElections m_manipulationElections;
        public ElectionsController(ManipulationElections p_manipulationElections)
        {
            if (p_manipulationElections == null)
            {
                throw new ArgumentNullException(nameof(p_manipulationElections));
            }

            this.m_manipulationElections = p_manipulationElections;
        }
        // GET: api/<ElectionsController>
        [HttpGet]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public ActionResult<IEnumerable<Models.Election>> Get(int municipaliteId)
        {
            List<Models.Election> elections = new List<Models.Election>();

            try
            {
                elections = this.m_manipulationElections.RechercherElectionsPourMunicipalite(municipaliteId)
                                                                          .Select(election => new Models.Election(election))
                                                                          .ToList();
            }
            catch (ArgumentNullException)
            {
                return NotFound();
            }

            if (elections.Count > 0)
            {
                return Ok(elections);
            }

            return NotFound();
        }

        // GET api/<ElectionsController>/5
        [HttpGet("{electionId}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public ActionResult<Models.Election> Get(int municipaliteId, int electionId)
        {
            Services.Election election = null;

            try
            {
                election = this.m_manipulationElections.RechercherElectionParIdentifiant(municipaliteId, electionId);
            }
            catch(ArgumentNullException)
            {
                return NotFound();
            }

            if (election != null)
            {
                Models.Election electionMODEL = new Models.Election(election);

                return Ok(electionMODEL);
            }

            return NotFound();
        }

        // POST api/<ElectionsController>
        [HttpPost]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        public ActionResult Post(int municipaliteId, [FromBody] Models.Election election)
        {
            if (!ModelState.IsValid
                || municipaliteId != election.MunicipaliteId
                || election.DateElection == null)
            {
                return BadRequest();
            }

            int electionIdMax = this.m_manipulationElections.ListerElections()
                                                            .OrderByDescending(election => election.ElectionId)
                                                            .FirstOrDefault()?.ElectionId ?? 0;
            election.ElectionId = electionIdMax + 1;
            Services.Election electionSRV = election.VersEntite();

            try
            {
                this.m_manipulationElections.AjouterElection(electionSRV);
            }
            catch (ArgumentNullException)
            {
                return BadRequest();
            }
            catch (ArgumentOutOfRangeException)
            {
                return BadRequest();
            }

            return CreatedAtAction(nameof(Get), new { id = election.ElectionId }, election);
        }

        // PUT api/<ElectionsController>/5
        [HttpPut("{electionId}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public ActionResult Put(int municipaliteId, int electionId, [FromBody] Models.Election election)
        {
            if (!ModelState.IsValid
                || election.ElectionId != electionId
                || election.DateElection == null)
            {
                return BadRequest();
            }

            try
            {
                int index = this.m_manipulationElections.RechercherElectionParIdentifiant(municipaliteId, electionId)?.ElectionId ?? -1;
            }
            catch (ArgumentNullException)
            {
                return NotFound();
            }

            Services.Election electionSRV = election.VersEntite();

            try
            {
                this.m_manipulationElections.MAJElection(electionSRV);
            }
            catch(ArgumentNullException)
            {
                return BadRequest();
            }
            catch (ArgumentOutOfRangeException)
            {
                return BadRequest();
            }

            return NoContent();
        }

        // DELETE api/<ElectionsController>/5
        [HttpDelete("{electionId}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        public ActionResult Delete(int municipaliteId, int electionId)
        {
            Services.Election election = null;

            try
            {
                election = this.m_manipulationElections.RechercherElectionParIdentifiant(municipaliteId, electionId);
            }
            catch (ArgumentNullException)
            {
                return NotFound();
            }

            if (election != null)
            {
                this.m_manipulationElections.DesactiverElection(election);

                return NoContent();
            }

            return NotFound();
        }
    }
}
