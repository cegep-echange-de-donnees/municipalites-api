﻿namespace DSED_M03_REST01.Models
{
    public class Election
    {
        public int ElectionId { get; set; }
        public int MunicipaliteId { get; set; }
        public DateTime? DateElection { get; set; }
        public bool Actif { get; set; }
        public Election()
        {
            ;
        }
        public Election(Services.Election p_election)
        {
            if (p_election == null)
            {
                throw new ArgumentNullException(nameof(p_election));
            }

            this.ElectionId = p_election.ElectionId;
            this.MunicipaliteId = p_election.MunicipaliteId;
            this.DateElection = p_election.DateElection;
            this.Actif = p_election.Actif;
        }
        public Services.Election VersEntite()
        {
            return new Services.Election(this.ElectionId, this.MunicipaliteId, this.DateElection, this.Actif);
        }
    }
}
