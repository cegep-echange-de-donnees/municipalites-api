﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace DSED_M03_REST01.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public DbSet<Municipalite> Municipalites { get; set; }
        public DbSet<Election> Elections { get; set; }
        public DbSet<ClefAPI> Clefs { get; set; }
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
    }
}