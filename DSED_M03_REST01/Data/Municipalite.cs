﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSED_M03_REST01.Data
{
    [Table("Municipalite")]
    public class Municipalite
    {
        [Key]
        public int MunicipaliteId { get; set; }
        [Column]
        public string Nom { get; set; }
        [Column]
        public string Courriel { get; set; }
        [Column]
        public string? SiteWeb { get; set; }
        [Column]
        public DateTime? DateProchaineElection { get; set; }
        [Column]
        public bool Actif { get; set; }
        public List<Election>? Elections { get; set; }
        public Municipalite()
        {
            ;
        }
        public Municipalite (Services.Municipalite p_municipalite)
        {
            if(p_municipalite == null)
            {
                throw new ArgumentNullException(nameof(p_municipalite));
            }
            this.MunicipaliteId = p_municipalite.MunicipaliteId;
            this.Nom = p_municipalite.Nom;
            this.Courriel = p_municipalite.Courriel;
            
            if(p_municipalite.SiteWeb != null)
            {
                this.SiteWeb = p_municipalite.SiteWeb;
            }
            if(p_municipalite.DateProchaineElection != null)
            {
                this.DateProchaineElection = p_municipalite.DateProchaineElection;
            }

            this.Actif = p_municipalite.Actif;
            this.Elections = p_municipalite.Elections?.Select(election => new Election(election)).ToList();
        }
        public Services.Municipalite VersEntite()
        {
            return new Services.Municipalite(this.MunicipaliteId, this.Nom, this.Courriel, this.SiteWeb, this.DateProchaineElection, this.Actif, this.Elections?.Select(election => election.VersEntite()).ToList());
        }
    }
}
