﻿using DSED_M03_REST01.Services;
using DSED_M03_REST01.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSED_M03_REST01.Data
{
    public class DepotMunicipalites : IDepotMunicipalites
    {
        private ApplicationDbContext m_applicationDbContext { get; set; }
        public DepotMunicipalites(ApplicationDbContext p_applicationDbContext)
        {
            if(p_applicationDbContext == null)
            {
                throw new ArgumentNullException(nameof(p_applicationDbContext));
            }
            m_applicationDbContext = p_applicationDbContext;
        }
        public void AjouterMunicipalite(Services.Municipalite p_municipalite)
        {
            Municipalite municipalite = new Municipalite(p_municipalite);
            this.m_applicationDbContext.Municipalites.Add(municipalite);
            this.m_applicationDbContext.SaveChanges();
            this.m_applicationDbContext.ChangeTracker.Clear();
        }

        public Services.Municipalite ChercherMunicipaliteParCodeGeographique(int p_municipaliteId)
        {
            return this.m_applicationDbContext.Municipalites.Where(municipalite => municipalite.MunicipaliteId == p_municipaliteId)
                                                        .Select(municipalite => municipalite.VersEntite()).FirstOrDefault();
        }

        public void DesactiverMunicipalite(Services.Municipalite p_municipalite)
        {
            p_municipalite.Actif = false;
            this.MAJMunicipalite(p_municipalite);
        }

        public IEnumerable<Services.Municipalite> ListerMunicipalitesActives()
        {
            IQueryable<Municipalite> requete = this.m_applicationDbContext.Municipalites;
            return requete.Where(municipalite => municipalite.Actif == true)
                          .Select(municipalite => municipalite.VersEntite()).ToList();
        }

        public void MAJMunicipalite(Services.Municipalite p_municipalite)
        {
            Municipalite municipalite = new Municipalite(p_municipalite);
            this.m_applicationDbContext.Municipalites.Update(municipalite);
            this.m_applicationDbContext.SaveChanges();
            this.m_applicationDbContext.ChangeTracker.Clear();
        }
    }
}
