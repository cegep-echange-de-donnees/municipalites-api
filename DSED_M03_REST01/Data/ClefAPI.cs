﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DSED_M03_REST01.Data
{
    [Table("ClefAPI")]
    public class ClefAPI
    {
        [Key]
        public Guid ClefAPIId { get; set; }
        public ClefAPI()
        {
            ;
        }
        public ClefAPI(Services.ClefAPI p_clefAPI)
        {
            if(p_clefAPI == null)
            {
                throw new ArgumentNullException(nameof(p_clefAPI));
            }
            this.ClefAPIId = p_clefAPI.ClefAPIId;
        }
        public override bool Equals(object? obj)
        {
            bool egal = false;

            if(!(obj is ClefAPI))
            {
                egal = false;
            }

            ClefAPI? autre = obj as ClefAPI;

            if(autre != null && autre.ClefAPIId == this.ClefAPIId)
            {
                egal = true ;
            }
            
            return egal;
        }
    }
}
