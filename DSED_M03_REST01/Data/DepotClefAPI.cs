﻿using DSED_M03_REST01.Services;

namespace DSED_M03_REST01.Data
{
    public class DepotClefAPI : IDepotClefAPI
    {
        private ApplicationDbContext m_applicationDbContext { get; set; }
        public DepotClefAPI(ApplicationDbContext p_applicationDbContext)
        {
            if (p_applicationDbContext == null)
            {
                throw new ArgumentNullException(nameof(p_applicationDbContext));
            }
            m_applicationDbContext = p_applicationDbContext;
        }
        public bool ValiderClefAPI(Services.ClefAPI p_clefAPI)
        {
            ClefAPI clefAPI = new ClefAPI(p_clefAPI);
            IQueryable<ClefAPI> requete = this.m_applicationDbContext.Clefs;
            return requete.Any(clef => clef.Equals(clefAPI));
        }
    }
}
