﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DSED_M03_REST01.Data
{
    [Table("Election")]
    public class Election
    {
        [Key]
        public int ElectionId { get; set; }
        [Column]
        [ForeignKey("MunicipaliteId")]
        public int MunicipaliteId { get; set; }
        [Column]
        [Required]
        public DateTime? DateElection { get; set; }
        [Column]
        public bool Actif { get; set; }
        public Election()
        {
            ;
        }
        public Election(Services.Election p_election)
        {
            if(p_election == null)
            {
                throw new ArgumentNullException(nameof(p_election));
            }

            this.ElectionId = p_election.ElectionId;
            this.MunicipaliteId = p_election.MunicipaliteId;
            this.DateElection = p_election.DateElection;
            this.Actif = p_election.Actif;
        }
        public Services.Election VersEntite()
        {
            return new Services.Election(this.ElectionId, this.MunicipaliteId, this.DateElection, this.Actif);
        }
    }
}
