﻿using DSED_M03_REST01.Services;

namespace DSED_M03_REST01.Data
{
    public class DepotElections : IDepotElections
    {
        private ApplicationDbContext m_applicationDbContext { get; set; }
        public DepotElections(ApplicationDbContext p_applicationDbContext)
        {
            if (p_applicationDbContext == null)
            {
                throw new ArgumentNullException(nameof(p_applicationDbContext));
            }
            m_applicationDbContext = p_applicationDbContext;
        }
        public void AjouterElection(Services.Election p_election)
        {
            Election election = new Election(p_election);
            this.m_applicationDbContext.Elections.Add(election);
            this.m_applicationDbContext.SaveChanges();
            this.m_applicationDbContext.ChangeTracker.Clear();
        }

        public void DesactiverElection(Services.Election p_election)
        {
            p_election.Actif = false;
            this.MAJElection(p_election);
        }

        public void MAJElection(Services.Election p_election)
        {
            Election election = new Election(p_election);
            this.m_applicationDbContext.Elections.Update(election);
            this.m_applicationDbContext.SaveChanges();
            this.m_applicationDbContext.ChangeTracker.Clear();
        }

        public Services.Election RechercherElectionParIdentifiant(int p_electionId)
        {
            Services.Election? election = this.m_applicationDbContext.Elections.Where(election => election.ElectionId == p_electionId)
                                                        .Select(election => election.VersEntite()).SingleOrDefault();

            return election;
        }

        public IEnumerable<Services.Election> RechercherElectionsPourMunicipalite(int p_municipaliteId)
        {
            IQueryable<Election> requete = this.m_applicationDbContext.Elections;
            return requete.Where(election => election.Actif == true && election.MunicipaliteId == p_municipaliteId)
                          .Select(election => election.VersEntite()).ToList();
        }
        public IEnumerable<Services.Election> ListerElections()
        {
            IQueryable<Election> requete = this.m_applicationDbContext.Elections;
            return requete.Select(election => election.VersEntite()).ToList();
        }
    }
}
