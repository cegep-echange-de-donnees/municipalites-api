﻿using DSED_M03_REST01.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Primitives;

namespace DSED_M03_REST01.Filters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class ApiKeyAttribute : Attribute, IAsyncActionFilter
    {
        public async Task OnActionExecutionAsync(ActionExecutingContext p_context, ActionExecutionDelegate p_next)
        {
            IDepotClefAPI? depotClefAPI = p_context.HttpContext.RequestServices.GetService<IDepotClefAPI>();
            StringValues clefAPI;

            if (!p_context.HttpContext.Request.Headers.TryGetValue("clefAPI", out clefAPI))
            {
                p_context.Result = new UnauthorizedResult();
                return;
            }

            Services.ClefAPI clef = new Services.ClefAPI(Guid.Parse(clefAPI));

            if (depotClefAPI == null || (!(depotClefAPI.ValiderClefAPI(clef))))
            {
                p_context.Result = new UnauthorizedResult();
                return;
            }
            // Excécute la suite des filtres
            await p_next();
        }
    }
}
